package com.mycompany.app.assignmenttwo.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {

    public Properties getProperties() throws IOException {
        InputStream propertiesStream = this.getClass().getClassLoader().getResourceAsStream("application.properties");
        Properties properties = new Properties();
        properties.load(propertiesStream);

        return properties;
    }
}
