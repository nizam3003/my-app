package com.mycompany.app.assignmenttwo;

import java.io.*;
import java.sql.*;
import java.util.Properties;

public class DatabaseConnection {

    public Properties getProperties() throws IOException {
        InputStream propertiesStream = this.getClass().getClassLoader().getResourceAsStream("application.properties");
        Properties properties = new Properties();
        properties.load(propertiesStream);

        return properties;
    }

    public Connection getConnection() throws ClassNotFoundException {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        // Open a connection
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Properties properties = databaseConnection.getProperties();

            Connection conn = DriverManager.getConnection(properties.getProperty("jdbc.url"), properties.getProperty("user.name"), properties.getProperty("password"));
            Statement stmt = conn.createStatement();

//         String insertData = "insert into mysql(" +"Id, Name) Values" + "('3', 'NizamKhan')";

//         int resultSet = stmt.executeUpdate(insertData);

            ResultSet resultSet = (ResultSet) stmt.executeQuery("select * from Student");
            while (resultSet.next())
                System.out.println(resultSet.getInt(1) + "  " + resultSet.getString(2));
            conn.close();
//         System.out.println("Row is inserted" +resultSet);

//         System.out.println("Created table in given database...");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    public ResultSet executeQuery(String sql) {
        ResultSet resultSet = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultSet;
    }
}
