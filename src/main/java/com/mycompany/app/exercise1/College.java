package com.mycompany.app.exercise1;

import java.util.List;
import java.util.Objects;

public class College {

    public String name;
    public String Address;
    List<java.lang.Class> list;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        College college = (College) o;
        return Objects.equals(name, college.name) && Objects.equals(Address, college.Address) && Objects.equals(list, college.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, Address, list);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public List<java.lang.Class> getList() {
        return list;
    }

    public void setList(List<java.lang.Class> list) {
        this.list = list;
    }
}


