package com.mycompany.app.exercise1;

import java.util.Objects;
import java.util.Set;

public class Class {

    public String className;
    public String noOfStudents;
    Set<Student> set;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Class aClass = (Class) o;
        return Objects.equals(noOfStudents, aClass.noOfStudents) && Objects.equals(className, aClass.className) && Objects.equals(set, aClass.set);
    }

    @Override
    public int hashCode() {
        return Objects.hash(className, noOfStudents, set);
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getNoOfStudents() {
        return noOfStudents;
    }

    public void setNoOfStudents(String noOfStudents) {
        this.noOfStudents = noOfStudents;
    }

    public Set<Student> getStd() {
        return set;
    }

    public void setStd(Set<Student> std) {
        this.set = std;
    }
}
