package com.mycompany.app.exercise2;

public class Main {

    public static void main(String[] args){
        Flyable f1 = new Fly();
        Quack q1 = new Quacking();
        Flyable f2 = new NotFly();
        Quack q2 = new NotQuacking();

        Duck duck = new WhiteDuck(f1,q1);
        Duck duck1 = new BrownDuck(f1,q1);
        Duck duck2 = new Mallard(f1,q2);
        Duck duck3 = new RubberDuck(f2,q1);
        Duck duck4 = new DecoyDuck(f2,q2);

        duck.fly();
        duck1.fly();
        duck3.fly();
    }

}
