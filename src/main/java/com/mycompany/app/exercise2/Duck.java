package com.mycompany.app.exercise2;

abstract class Duck {

    Flyable fy;
    Quack qck;

    public Duck(Flyable fy, Quack qck) {
        super();
        this.fy = fy;
        this.qck = qck;
    }

    public Duck(Flyable fy) {
        super();
        this.fy = fy;
    }



    public void setFy(Flyable fy) {
        this.fy = fy;
    }


    public void setQck(Quack qck) {
        this.qck = qck;
    }

    public void fly() {
        fy.fly();
    }

    public void quack() {
        qck.quack();
    }



}


