package com.mycompany.app.exercise2;

public class NotQuacking implements Quack {

    @Override
    public void quack() {
        System.out.println("I cannot quack");

    }

}
